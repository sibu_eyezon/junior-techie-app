<section class="service-area bg_cover" style="background-image: url(<?= site_url(); ?>assets/images/newsletter-bg.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8">
                <div class="section-title wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                    <h6 class="sub-title">Top Courses</h6>
                    <!-- <h4 class="title">The 1st intelligence and collaboration for the <span>digital marketing agency.</span></h4> -->
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->
        <div class="service-wrapper mt-60 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
            <div class="row no-gutters justify-content-center">
                <div class="col-lg-4 col-md-7">
                    <div class="single-service d-flex">
                        <div class="service-icon">
                            <img src="<?= site_url(); ?>assets/images/python.png" alt="Icon">
                        </div>
                        <div class="service-content media-body">
                            <h4 class="service-title">Basic Python</h4>
                            <p class="text">Python is one of the most popular program in present era. it is easy to learn, interpreted high-level general-purpose programming language.</p>
							<button type="button" onClick="book_trial_class('Basic Python')" class="btn btn-primary btn-round">Book a Free Trial</button>
                        </div>
                        <div class="shape shape-1">
                            <img src="<?= site_url(); ?>assets/images/shape/gear.png" alt="shape">
                        </div>
                        <div class="shape shape-2">
                            <img src="<?= site_url(); ?>assets/images/shape/binary-code.png" alt="shape">
                        </div>
                    </div> <!-- single service -->
                </div>
                <div class="col-lg-4 col-md-7">
                    <div class="single-service service-border d-flex">
                        <div class="service-icon">
                            <img src="<?= site_url(); ?>assets/images/robot.png" alt="Icon">
                        </div>
                        <div class="service-content media-body">
                            <h4 class="service-title">Robotics</h4>
                            <p class="text">Robotics is the future. Learning Basics of Robotics can help the students in interest toward the future technologies.</p>
							<button type="button" onClick="book_trial_class('Robotics')" class="btn btn-primary btn-round">Book a Free Trial</button>
                        </div>
                        <div class="shape shape-3">
                            <img src="<?= site_url(); ?>assets/images/shape/coding.png" alt="shape">
                        </div>
                    </div> <!-- single service -->
                </div>
                <div class="col-lg-4 col-md-7">
                    <div class="single-service d-flex">
                        <div class="service-icon">
                            <img src="<?= site_url(); ?>assets/images/java.png" alt="Icon">
                        </div>
                        <div class="service-content media-body">
                            <h4 class="service-title">Java and C</h4>
                            <p class="text">Java as the third most popular languages and one of the main language in which Android is made. It is the first OOP. C is also most popular language and very good for the initial learning.</p>
							<button type="button" onClick="book_trial_class('Java and C')" class="btn btn-primary btn-round">Book a Free Trial</button>
                        </div>
                        <div class="shape shape-4">
                            <img src="<?= site_url(); ?>assets/images/shape/binary-code.png" alt="shape">
                        </div>
                        <div class="shape shape-5">
                            <img src="<?= site_url(); ?>assets/images/shape/coding-language.png" alt="shape">
                        </div>
                    </div> <!-- single service -->
                </div>
            </div> <!-- row -->
            <!-- <div class="row no-gutters justify-content-center">
                <div class="col-lg-4 col-md-7">
                    <div class="single-service d-flex">
                        <div class="service-icon">
                            <img src="assets/images/service-1.png" alt="Icon">
                        </div>
                        <div class="service-content media-body">
                            <h4 class="service-title">Course Name</h4>
                            <p class="text">Lorem Ipsum is simply dummy tex of the printing and typesetting industry. Lorem Ipsum .</p>
                        </div>
                        <div class="shape shape-1">
                            <img src="assets/images/shape/coding-language.png" alt="shape">
                        </div>
                        <div class="shape shape-2">
                            <img src="assets/images/shape/coding.png" alt="shape">
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-md-7">
                    <div class="single-service service-border d-flex">
                        <div class="service-icon">
                            <img src="assets/images/service-2.png" alt="Icon">
                        </div>
                        <div class="service-content media-body">
                            <h4 class="service-title">Course Name</h4>
                            <p class="text">Lorem Ipsum is simply dummy tex of the printing and typesetting industry. Lorem Ipsum .</p>
                        </div>
                        <div class="shape shape-3">
                            <img src="assets/images/shape/binary-code.png" alt="shape">
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-md-7">
                    <div class="single-service d-flex">
                        <div class="service-icon">
                            <img src="assets/images/service-3.png" alt="Icon">
                        </div>
                        <div class="service-content media-body">
                            <h4 class="service-title">Course Name</h4>
                            <p class="text">Lorem Ipsum is simply dummy tex of the printing and typesetting industry. Lorem Ipsum .</p>
                        </div>
                        <div class="shape shape-4">
                            <img src="assets/images/shape/shape-4.svg" alt="shape">
                        </div>
                        <div class="shape shape-5">
                            <img src="assets/images/shape/gear.png" alt="shape">
                        </div>
                    </div> 
                </div>
            </div> -->
            <!-- row <div class="row">
                <div class="col-lg-12">
                    <div class="service-btn text-center pt-25 pb-15">
                        <a class="main-btn main-btn-2" href="#">All Courses</a>
                    </div> <!-- service btn -->
                </div>
            </div> -->
        </div> <!-- service wrapper -->
    </div> <!-- container -->
</section>

<div id="why_us" style="padding-top:5em !important;"></div>
<section class="about-area-2 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-content-2 mt-45 wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="section-title">
                        <h6 class="sub-title">Why Us?</h6>
                        <!-- <h4 class="title">Hire us <br> let us <span>make it <br> happen.</span></h4> -->
                    </div> <!-- section title -->
                    <ul class="about-line">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <p class="text">
                        Duis et metus et massa tempus lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas ultricies, orci molestie blandit interdum. <br> <br> ipsum ante pellentesque nisl, eget mollis turpis quam nec eros. ultricies, orci molestie blandit interdum.
                    </p>
                    <div class="about-counter pt-60">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="single-counter counter-color-4 mt-30 d-flex">
                                    <div class="counter-shape">
                                        <span class="shape-1"></span>
                                        <span class="shape-2"></span>
                                    </div>
                                    <div class="counter-content media-body">
                                        <span class="counter-count"><span class="counter">35</span>%</span>
                                        <p class="text">Conversion Increase</p>
                                    </div>
                                </div> <!-- single counter -->
                            </div>
                            <div class="col-sm-6">
                                <div class="single-counter counter-color-5 mt-30 d-flex">
                                    <div class="counter-shape">
                                        <span class="shape-1"></span>
                                        <span class="shape-2"></span>
                                    </div>
                                    <div class="counter-content media-body">
                                        <span class="counter-count"><span class="counter">100</span>%</span>
                                        <p class="text">Satisfaction</p>
                                    </div>
                                </div> <!-- single counter -->
                            </div>
                        </div> <!-- row -->
                    </div> <!-- about counter -->
                </div> <!-- about content -->
            </div> <!-- about image -->
            <div class="col-lg-6">
                <div class="about-image-2 text-center mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="assets/images/about-2.jpg" alt="about">
                </div> <!-- about image -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<div id="why_coding" style="padding-top:5em !important;"></div>
<section class="about-area-2 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-image-2 text-center mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="assets/images/about-2.jpg" alt="about">
                </div> <!-- about image -->
            </div>
            <div class="col-lg-6">
                <div class="about-content-2 mt-45 wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="section-title">
                        <h6 class="sub-title">Why Coding?</h6>
                        <!-- <h4 class="title">Hire us <br> let us <span>make it <br> happen.</span></h4> -->
                    </div> <!-- section title -->
                    <ul class="about-line">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <p class="text">
                        Duis et metus et massa tempus lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas ultricies, orci molestie blandit interdum. <br> <br> ipsum ante pellentesque nisl, eget mollis turpis quam nec eros. ultricies, orci molestie blandit interdum.
                    </p>
                    <div class="about-counter pt-60">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="single-counter counter-color-4 mt-30 d-flex">
                                    <div class="counter-shape">
                                        <span class="shape-1"></span>
                                        <span class="shape-2"></span>
                                    </div>
                                    <div class="counter-content media-body">
                                        <span class="counter-count"><span class="counter">35</span>%</span>
                                        <p class="text">Conversion Increase</p>
                                    </div>
                                </div> <!-- single counter -->
                            </div>
                            <div class="col-sm-6">
                                <div class="single-counter counter-color-5 mt-30 d-flex">
                                    <div class="counter-shape">
                                        <span class="shape-1"></span>
                                        <span class="shape-2"></span>
                                    </div>
                                    <div class="counter-content media-body">
                                        <span class="counter-count"><span class="counter">100</span>%</span>
                                        <p class="text">Satisfaction</p>
                                    </div>
                                </div> <!-- single counter -->
                            </div>
                        </div> <!-- row -->
                    </div> <!-- about counter -->
                </div> <!-- about content -->
            </div> <!-- about image -->
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<div id="why_robotics" style="padding-top:5em !important;"></div>
<section class="about-area-2 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-content-2 mt-45 wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="section-title">
                        <h6 class="sub-title">Why Robotics?</h6>
                        <!-- <h4 class="title">Hire us <br> let us <span>make it <br> happen.</span></h4> -->
                    </div> <!-- section title -->
                    <ul class="about-line">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <p class="text">
                        Duis et metus et massa tempus lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas ultricies, orci molestie blandit interdum. <br> <br> ipsum ante pellentesque nisl, eget mollis turpis quam nec eros. ultricies, orci molestie blandit interdum.
                    </p>
                    <div class="about-counter pt-60">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="single-counter counter-color-4 mt-30 d-flex">
                                    <div class="counter-shape">
                                        <span class="shape-1"></span>
                                        <span class="shape-2"></span>
                                    </div>
                                    <div class="counter-content media-body">
                                        <span class="counter-count"><span class="counter">35</span>%</span>
                                        <p class="text">Conversion Increase</p>
                                    </div>
                                </div> <!-- single counter -->
                            </div>
                            <div class="col-sm-6">
                                <div class="single-counter counter-color-5 mt-30 d-flex">
                                    <div class="counter-shape">
                                        <span class="shape-1"></span>
                                        <span class="shape-2"></span>
                                    </div>
                                    <div class="counter-content media-body">
                                        <span class="counter-count"><span class="counter">100</span>%</span>
                                        <p class="text">Satisfaction</p>
                                    </div>
                                </div> <!-- single counter -->
                            </div>
                        </div> <!-- row -->
                    </div> <!-- about counter -->
                </div> <!-- about content -->
            </div> <!-- about image -->
            <div class="col-lg-6">
                <div class="about-image-2 text-center mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="assets/images/about-2.jpg" alt="about">
                </div> <!-- about image -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</section>