<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $pageTitle; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= site_url(); ?>assets/images/favicon.ico" type="image/ico">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/slick.css">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/LineIcons.css">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/animate.css">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/default.css">
    <link rel="stylesheet" href="<?= site_url(); ?>assets/css/style.css">
	<script src="<?= site_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="<?= site_url(); ?>assets/js/jquery.form-validator.min.js"></script>
	<script src="<?= site_url(); ?>assets/js/vendor/modernizr-3.7.1.min.js"></script>
	<script src="<?= site_url(); ?>assets/js/popper.min.js"></script>
	<script src="<?= site_url(); ?>assets/js/bootstrap.min.js"></script>
	<script>var baseURL = '<?= base_url(); ?>';</script>
    
</head>
<body>
<!--====== PRELOADER PART START ======-->

<div class="preloader">
    <div class="loader">
        <div class="ytp-spinner">
            <div class="ytp-spinner-container">
                <div class="ytp-spinner-rotator">
                    <div class="ytp-spinner-left">
                        <div class="ytp-spinner-circle"></div>
                    </div>
                    <div class="ytp-spinner-right">
                        <div class="ytp-spinner-circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--====== PRELOADER PART ENDS ======-->

<?php
    include('includes/header.php');
    include('pages/'.$pageName.'.php');
    include('includes/footer.php');
?>
<div class="modal fade text-left" id="book_trial_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Book a Trial Class</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" id="frmBook" onSubmit="func_book_trial(event);">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>First Name</label>
								<input type="text" name="first_name" id="first_name" class="form-control"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" name="last_name" id="last_name" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Gaurdian Full Name</label>
								<input type="text" name="gaurdian_name" id="gaurdian_name" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Gaurdian's Email</label>
								<input type="text" name="email" id="email" class="form-control"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Gaurdian's Mobile</label>
								<input type="text" name="phone" id="phone" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Subject</label>
								<input type="text" name="subject" id="subject" class="form-control" value="" readonly/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Message </label>
								<textarea name="message" id="message" class="form-control" rows="5" cols="80"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">
							<button type="submit" id="btn_book" class="btn btn-danger btn-md">Book Now</button>
							<input type="reset" style="visibility:hidden;"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	
	function func_book_trial(e)
	{
		e.preventDefault();
		var frmdata = new FormData($('#frmBook')[0]);
		$.ajax({
			beforeSend: ()=>{
				$('#btn_book').html('<i class="fa fa-spinner fa-spin"></i> Loading');
				$('#btn_book').attr('disabled', 'disabled');
			},
			url: baseURL+'home/book_trial_class',
			type: 'POST',
			contentType: false,
			processData: false,
			data: frmdata,
			success: (resp)=>{
				$('#book_trial_modal').modal('hide');
				$('#frmBook')[0].reset();
				$('#btn_book').html('Book Now');
				$('#btn_book').removeAttr('disabled');
				var obj = JSON.parse(resp);
				if(obj['status']){
					Swal.fire(
					  'Success!',
					  'Your booking has been created. You will contacted surely.',
					  'success'
					);
				}else{
					Swal.fire(
					  'Error!',
					  obj['msg'],
					  'warning'
					);
				}
			},
			error: (err)=>{
				//console.log(err)
				$('#btn_book').html('Book Now');
				$('#btn_book').removeAttr('disabled');
				$('#book_trial_modal').modal('hide');
				$('#frmBook')[0].reset();
				Swal.fire(
				  'Error!',
				  'Server error. Please try again.',
				  'warning'
				);
			}
		});
	}
	function book_trial_class(subject)
	{
		$('#frmBook')[0].reset();
		$('#frmBook #subject').val('Book a trial class');
		if(subject != ""){
			$('#frmBook #subject').val('Book a trial class for: '+subject);
		}
		$('#book_trial_modal').modal('show');
	}
</script>
<!--====== Jquery js ======-->

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="<?= site_url(); ?>assets/js/main.js"></script>
<script src="<?= site_url(); ?>assets/js/custom.js"></script>
<script src="<?= site_url(); ?>assets/js/plugins.js"></script>
<script src="<?= site_url(); ?>assets/js/slick.min.js"></script>
<script src="<?= site_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
<script src="<?= site_url(); ?>assets/js/isotope.pkgd.min.js"></script>
<script src="<?= site_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?= site_url(); ?>assets/js/waypoints.min.js"></script>
<script src="<?= site_url(); ?>assets/js/jquery.counterup.min.js"></script>
<script src="<?= site_url(); ?>assets/js/circles.min.js"></script>
<script src="<?= site_url(); ?>assets/js/jquery.appear.min.js"></script>
<script src="<?= site_url(); ?>assets/js/wow.min.js"></script>
</body>
</html>