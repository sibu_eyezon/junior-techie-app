<!--====== HEADER PART START ======-->
    
<header class="header-area">
    <div class="navbar-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" style="width: 20% !important;" href="<?= site_url(); ?>">
                            <img src="<?= site_url(); ?>assets/images/logo.png" alt="Logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                            <ul class="navbar-nav m-auto">
                                <li class="nav-item active">
                                    <a href="<?= site_url(); ?>">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#why_us">Why Us? </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#why_coding">Why Coding?</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#why_robotics">Why Robotics?</a>
                                </li>
                            </ul>
                        </div> <!-- navbar collapse -->
                        
                        <div class="navbar-btn d-none d-lg-inline-block">
                            <a class="main-btn" href="javascript:book_trial_class('');">Book a Trial Class</a>
                        </div>
                    </nav> <!-- navbar -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- navbar area -->
    
    <div class="header-hero bg_cover d-lg-flex align-items-center" style="background-image: url(assets/images/header-hero.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="header-hero-content">
                        <h3 class="hero-title wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s"><b>Be Smarter with</b> <span>Coding,</span> Robotics, <b>and more.</b></h3>
                        <!-- <p class="text wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Phasellus vel elit efficitur, gravida libero sit amet, scelerisque  tortor arcu, commodo sit amet nulla sed.</p> -->
                        <div class="header-singup wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
                            <button type="button" onClick="book_trial_class('')" class="btn btn-primary btn-round">Book a Free Trial</button>
                            <!--<button class="btn btn-primary btn-round">Sign Up</button>-->
                        </div>
                    </div> <!-- header hero content -->
                </div>
                <div class="col-lg-5">
					<figure class="w-100 ie-height-600">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 678 595" style="enable-background:new 0 0 678 595;" xml:space="preserve">
					<defs>
					<linearGradient id="BgGradient">
						<stop class="fill-grad-start" offset="0%"/>
						<stop class="fill-grad-end" offset="100%"/>
					</linearGradient>
					</defs>
					<path id="XMLID_4_" fill="url(#BgGradient)" d="M652.2,493.6c-51.9,58.8-395.9,51.9-395.9,51.9S7.4,581.8,50.6,396.8
						C82.3,261.5,2.2,56.3,97.3,20c49-18.7,351.7-49,487.8,70.9C713.1,203.4,677.3,465.1,652.2,493.6z"/>
						<g id="XMLID_120_">
							<defs>
							<path id="XMLID_7_" d="M635.5,506.9c-51.9,58.8-395.9,51.9-395.9,51.9S-9.3,595,34,410C65.6,274.7-14.4,69.5,80.6,33.2
								c49-18.7,351.7-49,487.8,70.9C696.4,216.7,660.6,478.4,635.5,506.9z"/>
								</defs>
								<clipPath id="XMLID_155_">
								<use xlink:href="#XMLID_7_"  style="overflow:visible;"/>
								</clipPath>
								<g style="clip-path:url(#XMLID_155_);">
									
									<image style="overflow:visible;" width="700" height="700" id="XMLID_121_" xlink:href="assets/images/02.jpg"  transform="matrix(0.9999 0 0 0.9999 1 -81)">
									</image>
								</g>
							</g>
							<circle fill="none" stroke="url(#BgGradient)" stroke-width="8" stroke-miterlimit="10" cx="60" cy="539" r="40"/>
							<path id="XMLID_149_" fill="url(#BgGradient)" d="M52.6,104.3L25.5,46.5c-2-4.2-0.1-9.4,4.1-11.3l0,0c4.2-2,9.4-0.1,11.3,4.1L68,97c2,4.2,0.1,9.4-4.1,11.3
								l0,0C59.7,110.4,54.6,108.5,52.6,104.3z"/>
								<path id="XMLID_150_" fill="url(#BgGradient)" d="M48.1,140.6l-12.8-27.2c-2-4.2-0.1-9.4,4.1-11.3l0,0c4.2-2,9.4-0.1,11.3,4.1l12.8,27.2
									c2,4.2,0.1,9.4-4.1,11.3l0,0C55.2,146.7,50.1,144.8,48.1,140.6z"/>
									<circle id="XMLID_151_" fill="url(#BgGradient)" cx="590.9" cy="23.9" r="9.1"/>
									<circle id="XMLID_152_" fill="url(#BgGradient)" cx="596.8" cy="76.4" r="14.6"/>
									<circle id="XMLID_153_" fill="url(#BgGradient)" cx="630.8" cy="42.8" r="5.3"/>
								</svg>
					</figure>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- header hero -->
</header>

<!--====== HEADER PART ENDS ======-->