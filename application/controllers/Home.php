<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model('Member');
		ini_set('memory_limit', '-1');
	}
	public function index()
	{
        $data['pageTitle'] = 'Home | JuniorLancer';
        $data['pageName'] = 'home';
		$this->load->view('index', $data);
	}
	public function book_trial_class()
	{
		$this->load->library('form_validation');
		$resp = array('status'=>false, 'msg'=>'Something went wrong.');
		
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('gaurdian_name', 'Gaurdian Fullname', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_emails');
		$this->form_validation->set_rules('phone', 'Mobile', 'trim|required|numeric|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('message', 'Message', 'trim');
		
		if ($this->form_validation->run() == FALSE){
			$resp['msg'] = validation_errors();
		}else{
			$data['first_name'] = ucwords(strtolower(trim($_POST['first_name'])));
			$data['last_name'] = ucwords(strtolower(trim($_POST['last_name'])));
			$data['gaurdian_name'] = ucwords(strtolower(trim($_POST['gaurdian_name'])));
			$data['email'] = strtolower(trim($_POST['email']));
			$data['phone'] = trim($_POST['phone']);
			$data['subject'] = trim($_POST['subject']);
			$data['message'] = trim($_POST['message']);
			$data['add_datetime'] = date('Y-m-d H:i:s');
			if($this->Member->insertData('junior_trial_class', $data)){
				$resp['status'] = true;
			}else{
				$resp['msg'] = 'Server failure. Please try again.';
			}
		}
		echo json_encode($resp);
	}
}

?>