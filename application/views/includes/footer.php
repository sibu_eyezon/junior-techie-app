<!--====== FOOTER PART START ======-->
    
<footer class="footer-area bg_cover" style="background-image: url(<?= site_url(); ?>assets/images/footer-bg.jpg)">
    <div class="container">
        <div class="footer-widget pt-30 pb-70">
            <div class="row">
                <div class="col-lg-3 col-sm-6 order-sm-1 order-lg-1">
                    <div class="footer-about pt-40">
                        <a href="#">
                            <img src="<?= site_url(); ?>assets/images/logo.png" alt="Logo">
                        </a>
                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, repudiandae! Totam, nemo sed? Provident.</p>
                    </div> <!-- footer about -->
                </div>
                <div class="col-lg-3 col-sm-6 order-sm-3 order-lg-2">
                    <div class="footer-link pt-40">
                        <div class="footer-title">
                            <h5 class="title">Services</h5>
                        </div>
                        <ul>
                            <li><a href="#">Seo Services</a></li>
                            <li><a href="#">Virtual Marketing</a></li>
                            <li><a href="#">Web Analytics</a></li>
                            <li><a href="#">Web Development</a></li>
                        </ul>
                    </div> <!-- footer link -->
                </div>
                <div class="col-lg-3 col-sm-6 order-sm-4 order-lg-3">
                    <div class="footer-link pt-40">
                        <div class="footer-title">
                            <h5 class="title">About Us</h5>
                        </div>
                        <ul>
                            <!-- <li><a href="#">Overview</a></li>
                            <li><a href="#">Why us</a></li>
                            <li><a href="#">Awards & Recognitions</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Client Reviews</a></li> -->
                        </ul>
                    </div> <!-- footer link -->
                </div>
                <div class="col-lg-3 col-sm-6 order-sm-2 order-lg-4">
                    <div class="footer-contact pt-40">
                        <div class="footer-title">
                            <h5 class="title">Client Reviews</h5>
                        </div>
                        <div class="contact pt-10">
                            <ul class="social mt-40">
                                <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                                <li><a href="#"><i class="lni-twitter"></i></a></li>
                                <li><a href="#"><i class="lni-instagram-original"></i></a></li>
                                <li><a href="#"><i class="lni-linkedin"></i></a></li>
                            </ul>
                        </div> <!-- contact -->
                    </div> <!-- footer contact -->
                </div>
            </div> <!-- row -->
        </div> <!-- footer widget -->
        <div class="footer-copyright text-center">
            <p class="text">© 2022, All Rights Reserved.</p>
        </div>
    </div> <!-- container -->
</footer>

<!--====== FOOTER PART ENDS ======-->

<!--====== BACK TOP TOP PART START ======-->

<a href="#" class="back-to-top"><i class="lni-chevron-up"></i></a>

<!--====== BACK TOP TOP PART ENDS ======-->  